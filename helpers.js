const randomstring = require("randomstring")
const rp = require('request-promise')
const util = require('util')

module.exports = {
  toNumber: num => {
    try {
      return Number(num)
    } catch(e){
      console.error('Couldnt parse number: ', num, e)
      throw e
    }
  },
  getRating: () => 86 + Math.floor(Math.random() * 12),
  getId: () => randomstring.generate(20),
  splitTrim: (str, split) => str.trim().split(split).map(s => s.trim()),
  log: o => console.log(util.inspect(o, { depth: null, colors: true, maxArrayLength: null })),
  getGeocoding: address => {
    if(!address){
      console.error('no address!', address)
      throw 'geocoding'
    }
    const geolocationKey = process.env.GEOLOCATION_KEY
    return rp({
      uri: `https://maps.googleapis.com/maps/api/geocode/json?key=${geolocationKey}&address=${address}`,
      json: true
    }).then(response => {
      // console.log(util.inspect(response, false, null, true))
      if(response.status !== 'OK'){
        console.error('geocoding error', response)
        throw 'geocoding'
      }
      return {
        address: response.results[0].formatted_address.split(', USA')[0],
        location: {
          lat: response.results[0].geometry.location.lat,
          lng: response.results[0].geometry.location.lng
        }
      }
    }).catch(err => {
      console.error('geocoding error', err)
      throw 'geocoding'
    })
  },

  scrapeWebsite: str => {
    var matches = str.match(/href="([^\'\"]+)/g)
    if(!matches || !matches.length){
      console.error('couldnt scrape website!', str)
      throw 'website'
    }
    return matches[0].slice(6).toLowerCase()
  }
}
