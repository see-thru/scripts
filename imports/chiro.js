// node import.js
const { admin, db } = require('../clients/admin')
const GeoFirestore = require('geofirestore').GeoFirestore
const helpers = require('../helpers')
const fs = require('fs')
var parse = require('csv-parse')

var geoRef = db.collection('geostore')
const geoFirestore = new GeoFirestore(geoRef);

const services = {
  '1000000147': {
    name: 'Initial Visit (Assessment + Manipulation)',
    cptCode: 98940,
    time: 30
  },
  '1000000148': {
    name: 'Follow-up Treatment',
    cptCode: 98941,
    time: 30
  },
  '1000000149': {
    name: 'Manipulation',
    cptCode: 98942,
    time: 30
  },
  '1000000150': {
    name: 'Manipulation',
    cptCode: 98943,
    time: 30
  },
  '1000000151': {
    name: 'Evaluation and Management, Initial Visit',
    cptCode: 99202,
    time: 30
  },
  '1000000152': {
    name: 'Evaluation and Management, Initial Visit',
    cptCode: 99203,
    time: 30
  },
  '1000000153': {
    name: 'Evaluation and Management, Initial Visit',
    cptCode: 99204,
    time: 30
  },
  '1000000154': {
    name: 'Evaluation and Management, Established Patient',
    cptCode: 99212,
    time: 30
  },
  '1000000155': {
    name: 'Evaluation and Management, Established Patient',
    cptCode: 99213,
    time: 30
  },
  '1000000156': {
    name: 'Evaluation and Management, Established Patient',
    cptCode: 99214,
    time: 30
  },
  '1000000157': {
    name: 'Therapeutic Exercises',
    cptCode: 97110,
    time: 30
  },
  '1000000158': {
    name: 'Neuromuscular Re-education',
    cptCode: 97112,
    time: 30
  },
  '1000000159': {
    name: 'Manual Therapy',
    cptCode: 97140,
    time: 30
  },
  '1000000160': {
    name: 'Physical Performance Examination',
    cptCode: 97750,
    time: 30
  },
  '1000000161': {
    name: 'Additional X-ray'
  },
}
const serviceKeys = Object.keys(services)

function parseProviders(){
  return new Promise(resolve => {
    fs.readFile('./import-data/chiro-locations.csv', (err, fileData) => {
      if(err){ console.error('Error', err); return }
      parse(fileData, {columns: true, trim: true}, (err, rows) => {
        var start = 90
        var end = 100
        console.log(`slice ${start} - ${end}` )
        var providers = rows.slice(start, end).map(row => {
          console.log('parsing ' + row.ProvID)
          return helpers.getGeocoding(row.Address).then(office => {
            let provider = {
              providerInfo: {
                name: row.Name,
                photo: row.ImageLink,
                specialties: { primary: row.Specialty }
              },
              office,
              services: [],
              createdOn: admin.firestore.FieldValue.serverTimestamp()
            }
            if(row.Practice){
              provider.providerInfo.practiceName = row.Practice
            }
            if(row.Degree){
              provider.providerInfo.degrees = row.Degree
            }
            if(row.Phone){
              provider.providerInfo.phoneNumber = row.Phone
            }
            if(row.Website){
              provider.providerInfo.website = row.Website
            }
            if(row.Email){
              provider.providerInfo.email = row.Email
            }
            if(row.Notes){
              provider.providerInfo.notes = row.Notes
            }
            if(row.Description){
              provider.providerInfo.description = row.Description
            }
            if(row.Languages){
              provider.providerInfo.languages = helpers.splitTrim(row.Languages, ' ')
            }
            if(row.Training){
              provider.providerInfo.education = helpers.splitTrim(row.Training, ';')
            }

            serviceKeys.forEach(skey => {
              if(row[skey]){
                let service = Object.assign({ cost: helpers.toNumber(row[skey]) }, services[skey])
                provider.services.push(service)
              }
            })

            return provider
          })
        })

        Promise.all(providers).then(resolve)
      })
    })
  })
}

function addProviders(providers){
  if(!Array.isArray(providers)){
    console.error('Providers is not an array', providers)
    throw 'uhoh'
  }
  console.log(`adding ${ providers.length } providers`)

  let coordMap = {}
  var batch = db.batch()
  providers.forEach(provider => {
    var providerId = helpers.getId()
    console.log('creating provider ' + providerId)
    var newDoc = db.collection('providers').doc(providerId)
    batch.create(newDoc, provider)
    if(!provider.office.location){
      console.error('uhoh', provider)
      throw 'geolocation'
    }
    let coordinates = new admin.firestore.GeoPoint(provider.office.location.lat, provider.office.location.lng)
    let specialty = provider.providerInfo.specialties.primary
    coordMap[providerId] = { coordinates, specialty }
  })
  batch.commit().then(result => {
    console.log('BatchCommit: ', result, '-------')
    geoFirestore.set(coordMap).then(doc => {
      helpers.log(doc)
    })
  })
}

parseProviders().then(addProviders)
