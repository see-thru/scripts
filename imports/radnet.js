// node import.js
const { admin, db } = require('../clients/admin')
var GeoFirestore = require('geofirestore').GeoFirestore
const helpers = require('../helpers')
const fs = require('fs')
var parse = require('csv-parse')
var geoRef = db.collection('geostore')
const geoFirestore = new GeoFirestore(geoRef);

function getTimeFromCost(cost){
  if(cost < 50) return 15;
  if(cost < 100) return 30;
  if(cost < 200) return 45;
  else return 60;
}
function parsePrices(){
  fs.readFile('./import-data/radnet-prices.csv', (err, fileData) => {
    if(err){ console.error('Error', err); return }
    parse(fileData, {columns: true, trim: true}, (err, rows) => {
      if(err){ console.error('Error', err); return }
      let lists = {}
      rows.forEach(row => {
        let service = {
          name: row.Description.trim(),
          cost: helpers.toNumber(row.Rate),
          cptCode: row.Code,
          category: row.Category
        }
        service.time = getTimeFromCost(service.cost)
        if(!lists[row.Category]) lists[row.Category] = []
        lists[row.Category].push(service)
      })
      helpers.log(lists)
    })
  })
}

function parseProviders(){
  return new Promise(resolve => {
    var priceLists = require('../import-data/prices-parsed').radnet
    const categories = Object.keys(priceLists)
    fs.readFile('./import-data/radnet-locations.csv', (err, fileData) => {
      if(err){ console.error('Error', err); return }
      parse(fileData, {columns: true, trim: true}, (err, rows) => {
        var start = 270
        var end = 310
        console.log(`slice ${start} - ${end}` )
        var providers = rows.slice(start, end).map(row => {
          console.log('parsing ' + row.ProvID)
          return helpers.getGeocoding(row.Address).then(office => {
            let provider = {
              providerInfo: {
                name: row.Name,
                specialties: { primary: row.Specialty },
                rating: helpers.getRating(),
                photo: row.ImageLink
              },
              office,
              createdOn: admin.firestore.FieldValue.serverTimestamp()
            }
            if(row.Phone){
              provider.providerInfo.phoneNumber = row.Phone
            }
            if(row.Description){
              provider.providerInfo.description = row.Description
            }
            if(row.Languages){
              provider.providerInfo.languages = helpers.splitTrim(row.Languages, ';')
            }
            if(row.Training){
              provider.providerInfo.education = helpers.splitTrim(row.Training, '\n')
            }
            let serviceArrays = categories.filter(cat => row.Services.indexOf(cat) > -1)
              .map(cat => priceLists[cat])
            let services = [].concat.apply([], serviceArrays)
            provider.services = services
            return provider
          })
        })
        // providers = array[promise]
        Promise.all(providers).then(resolve)
      })
    })
  })
}

function addProviders(providers){
  if(!Array.isArray(providers)){
    console.error('Providers is not an array', providers)
    throw 'uhoh'
  }
  console.log(`adding ${ providers.length } providers`)

  let coordMap = {}
  var batch = db.batch()
  providers.forEach(provider => {
    var providerId = helpers.getId()
    console.log('creating provider ' + providerId)
    if(!provider.office.location){
      console.error('uhoh', provider)
      throw 'geolocation'
    }
    var newDoc = db.collection('providers').doc(providerId)
    batch.create(newDoc, provider)
    let coordinates = new admin.firestore.GeoPoint(provider.office.location.lat, provider.office.location.lng)
    let specialty = provider.providerInfo.specialties.primary
    coordMap[providerId] = { coordinates, specialty }
  })
  batch.commit().then(result => {
    console.log('BatchCommit: ', result, '-------')
    geoFirestore.set(coordMap).then(doc => {
      helpers.log(doc)
    })
  })
}

// parsePrices()
parseProviders().then(addProviders)
