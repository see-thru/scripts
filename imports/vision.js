// node import.js
const { admin, db } = require('../clients/admin')
var GeoFirestore = require('geofirestore').GeoFirestore
const helpers = require('../helpers')
const fs = require('fs')
var parse = require('csv-parse')
var geoRef = db.collection('geostore')
const geoFirestore = new GeoFirestore(geoRef);

const services = {
  '1000000136': {
    name: 'Comprehensive Eye Exam and Evaluation',
    time: 45,
    cptCode: 92004,
    isDefault: true,
    includes: [ 'Vision Test','Pupil Dilation','Initial Diagnosis','Treatment Program' ]
  },
  '1000000137': {
    name: 'Medical Examination and Evaluation with Initiation or Continuation of Diagnostic and Treatment Program; Comprehensive, Established Patient, One OR More Visits (with Dilation unless medically contraindicated)',
    time: 45,
    cptCode: 92014
  },
  '1000000138': {
    name: 'Medical Examination and Evaluation with Initiation or Continuation of Diagnostic and Treatment Program; Comprehensive, Established Patient, One OR More Visits',
    time: 45,
    cptCode: 92012
  },
  '1000000139': {
    name: 'Fluorescein angiography with interpretation and report',
    time: 45,
    cptCode: 92235,
    includes: [ 'Includes multiframe imaging' ]
  },
  '1000000140': {
    name: 'Prescription and fitting of contact lens',
    time: 45,
    cptCode: 92310,
    includes: [ 'Corneal lens','Both eyes','Excludes aphakia' ]
  },
  '1000000141': {
    name: 'Fundus photography with interpretation and report',
    time: 45,
    cptCode: 92250
  },
  '1000000142': {
    name: 'Extended threshold perimetry >= 3 Isopters. Full Threshold',
    time: 45,
    cptCode: 92083
  },
  '1000000143': {
    name: 'Intravitreal injection of a pharmacological agent',
    time: 45,
    cptCode: 67028
  },
  '1000000144': {
    name: 'Treatment of extensive or progressive retinopathy',
    time: 45,
    cptCode: 67228
  },
  '1000000145': {
    name: 'Destruction of localized lesion of retina (eg, macular edema, tumors), 1 or more sessions',
    time: 45,
    cptCode: 67210
  },
  '1000000146': {
    name: 'Repair of retinal detachment, 1 or more sessions',
    time: 45,
    cptCode: 67105
  }
}
const serviceKeys = Object.keys(services)

function parseProviders(){
  return new Promise(resolve => {
    fs.readFile('./import-data/vision-locations.csv', (err, fileData) => {
      if(err){ console.error('Error', err); return }
      parse(fileData, {columns: true, trim: true}, (err, rows) => {
        var start = 100
        var end = 150
        console.log(`slice ${start} - ${end}` )
        var providers = rows.slice(start, end).map(row => {
          console.log('parsing ' + row.ProvID)
          return helpers.getGeocoding(row.Address).then(office => {
            let provider = {
              providerInfo: {
                name: row.Name,
                photo: row.ImageLink,
                specialties: { primary: row.Specialty }
              },
              office,
              services: [],
              createdOn: admin.firestore.FieldValue.serverTimestamp()
            }
            if(row.Practice){
              provider.providerInfo.practiceName = row.Practice
            }
            if(row.Degree){
              provider.providerInfo.degrees = row.Degree
            }
            if(row.Phone){
              provider.providerInfo.phoneNumber = row.Phone
            }
            if(row.Website){
              provider.providerInfo.website = row.Website
            }
            if(row.Email){
              provider.providerInfo.email = row.Email
            }
            if(row.Notes){
              provider.providerInfo.notes = row.Notes
            }
            if(row.Description){
              provider.providerInfo.description = row.Description
            }
            if(row.Languages){
              provider.providerInfo.languages = helpers.splitTrim(row.Languages, ',')
            }
            if(row.Training){
              provider.providerInfo.education = helpers.splitTrim(row.Training, ';')
            }

            serviceKeys.forEach(skey => {
              if(row[skey]){
                let service = Object.assign({ cost: helpers.toNumber(row[skey]) }, services[skey])
                provider.services.push(service)
              }
            })

            return provider
          })
        })

        Promise.all(providers).then(resolve)
      })
    })
  })
}

function addProviders(providers){
  if(!Array.isArray(providers)){
    console.error('Providers is not an array', providers)
    throw 'uhoh'
  }
  console.log(`adding ${ providers.length } providers`)

  let coordMap = {}
  var batch = db.batch()
  providers.forEach(provider => {
    var providerId = helpers.getId()
    console.log('creating provider ' + providerId)
    var newDoc = db.collection('providers').doc(providerId)
    batch.create(newDoc, provider)
    if(!provider.office.location){
      console.error('uhoh', provider)
      throw 'geolocation'
    }
    let coordinates = new admin.firestore.GeoPoint(provider.office.location.lat, provider.office.location.lng)
    let specialty = provider.providerInfo.specialties.primary
    coordMap[providerId] = { coordinates, specialty }
  })
  batch.commit().then(result => {
    console.log('BatchCommit: ', result, '-------')
    geoFirestore.set(coordMap).then(doc => {
      helpers.log(doc)
    })
  })
}

parseProviders().then(addProviders)
