// node import.js
var { admin, db } = require('../clients/admin')
const GeoFirestore = require('geofirestore').GeoFirestore
const helpers = require('../helpers')
const fs = require('fs')
var parse = require('csv-parse')

var geoRef = db.collection('geostore')
const geoFirestore = new GeoFirestore(geoRef);

function parseProviders(){
  return new Promise(resolve => {
    fs.readFile('./import-data/ccphp-locations.csv', (err, fileData) => {
      if(err){ console.error('Error', err); return }
      parse(fileData, {columns: true, trim: true}, (err, rows) => {
        var providers = rows.map(row => {
          console.log('parsing ' + row.ProvID)
          return helpers.getGeocoding(row.Address).then(office => {
            let provider = {
              providerInfo: {
                name: row.Name,
                practiceName: row.Practice,
                degrees: row.Degree,
                specialties: {
                  primary: row.Primary,
                  secondary: helpers.splitTrim(row.Secondary, '|')
                },
                phoneNumber: row.Phone,
                photo: row.Image,
                languages: helpers.splitTrim(row.Languages, ','),
                rating: helpers.getRating(),
              },
              office,
              createdOn: admin.firestore.FieldValue.serverTimestamp()
            }
            let website = helpers.scrapeWebsite(row.Website)
            if(website){
              provider.providerInfo.website = website
            }
            if(row.Description){
              provider.providerInfo.description = row.Description
            }
            if(row.Training){
              provider.providerInfo.education = helpers.splitTrim(row.Training, '\n')
            }
            provider.services = [{
              name: row.Service,
              cost: helpers.toNumber(row.Price),
              time: helpers.toNumber(row.Time),
              includes: helpers.splitTrim(row.Includes),
              isDefault: true
            }]
            return provider
          })
        })

        Promise.all(providers).then(resolve)
      })
    })
  })
}

function addProviders(providers){
  if(!Array.isArray(providers)){
    console.error('Providers is not an array', providers)
    throw 'uhoh'
  }
  console.log(`adding ${ providers.length } providers`)

  let coordMap = {}
  var batch = db.batch()
  providers.forEach(provider => {
    var providerId = helpers.getId()
    console.log('creating provider ' + providerId)
    var newDoc = db.collection('providers').doc(providerId)
    batch.create(newDoc, provider)
    if(!provider.office.location){
      console.error('uhoh', provider)
      throw 'geolocation'
    }
    let coordinates = new admin.firestore.GeoPoint(provider.office.location.lat, provider.office.location.lng)
    let specialty = provider.providerInfo.specialties.primary
    coordMap[providerId] = { coordinates, specialty }
    // db.collection('providers').add(provider).then(ref => {
    //   console.log('Added: ' + ref.id)
    // })
  })
  batch.commit().then(result => {
    console.log('BatchCommit: ', result, '-------')
    geoFirestore.set(coordMap).then(doc => {
      helpers.log(doc)
    })
  })
}

const body = 'In a sea of impersonal doctors’ offices, how do you find a physician who truly understands your health and wellness needs in a setting where you can efficiently get those needs met? Membership with Edward S. Goldberg’s concierge practice is the answer. At his Upper East Side of Manhattan, New York, private medical practice, Dr. Edward S. Goldberg offers a refreshing alternative to the impersonal, rushed health care. As a concierge practice member, you benefit from 24/7 access to Dr. Goldberg and same-day office visits for urgent concerns. He coordinates every aspect of your care from routine screenings to vaccinations, and maintains front-line communication with all of your specialists.'
const education = `Bachelor of Arts: Biology with honors, Ithaca College
M.D.: UMDNJ Robert Wood Johnson Medical School, Piscataway, NJ
Internal Medicine Internship and Residency: Robert Wood Johnson University Hospital, New Brunswick, NJ
Chief Medical Resident: Robert Wood Johnson University Hospital, New Brunswick, NJ
Gastroenterology Fellowship: Memorial Sloan Kettering Cancer Center, New York
Board Certified American Board of Internal Medicine (ABIM)`
function addProvider(){
  return helpers.getGeocoding('121 East 60th St., #3C, New York, NY 10022').then(office => {
    let provider = {
      providerInfo: {
        name: 'Edward S. Goldberg',
        degrees: 'MD',
        specialties: {
          primary: 'Concierge Medicine',
          secondary: ['Internal Medicine','Gastroenterology','HIV Treatment','LGBT Care']
        },
        description: body,
        phoneNumber: '(212) 980-8800',
        photo: 'https://sa1s3optim.patientpop.com/assets/images/provider/photos/1845454.jpg',
        website: 'https://www.conciergemdny.com',
        languages: ['English'],
        education: helpers.splitTrim(education, '\n'),
        rating: helpers.getRating()
      },
      office,
      createdOn: admin.firestore.FieldValue.serverTimestamp()
    }
    provider.services = [{
      name: 'Free Consultation',
      cost: 0,
      time: 60,
      isDefault: true,
      includes: ['Meet with Dr. Goldberg to determine if his practice is the right fit for you']
    }, {
      name: 'Trial Membership',
      cost: 500,
      includes: helpers.splitTrim('2 weeks of Concierge Membership includes:;24/7 access to Dr. Goldberg via phone or email;Personalized, coordinated health care in one location, with one physician;Same-day visits for all urgent medical issues and most non-urgent conditions;Shorter waiting times and hassle-free care;Treatment for all adult medical concerns;Visits at your home or office upon request;Annual wellness exams;Detailed medical history;Evaluation of your lifestyle, quality of life, and risk factors for disease;Physical examinations;Preventive vaccinations for flu, pneumonia, and other diseases;Specialized evaluations like colonoscopies and endoscopies and CT screenings;Non-Invasive Laser Fat Removal using Sculpsure®', ';')
    }, {
      name: 'Yearly Membership',
      cost: 20000,
      includes: helpers.splitTrim('24/7 access to Dr. Goldberg via phone or email;Personalized, coordinated health care in one location, with one physician;Same-day visits for all urgent medical issues and most non-urgent conditions;Shorter waiting times and hassle-free care;Treatment for all adult medical concerns;Visits at your home or office upon request;Annual wellness exams;Detailed medical history;Evaluation of your lifestyle, quality of life, and risk factors for disease;Physical examinations;Preventive vaccinations for flu, pneumonia, and other diseases;Specialized evaluations like colonoscopies and endoscopies and CT screenings;Non-Invasive Laser Fat Removal using Sculpsure®', ';')
    }, {
      name: 'Sculpsure®',
      cost: 1500,
      time: 25,
      includes: ['1 treatment per area']
    }]
  
    if(!provider.office.location){
      console.error('uhoh', provider)
      throw 'geolocation'
    }
    db.collection('providers').add(provider).then(ref => {
      console.log('new provider: ', ref)
      var providerId = ref.id
      let coordinates = new admin.firestore.GeoPoint(provider.office.location.lat, provider.office.location.lng)
      let specialty = provider.providerInfo.specialties.primary
      let coordMap = {}
      coordMap[providerId] = { coordinates, specialty }
      geoFirestore.set(coordMap).then(doc => {
        helpers.log(doc)
      })
    })
  })
}

function removeProviders(providerIds){
  var batch = db.batch()
  providerIds.forEach(pid => {
    batch.delete(db.collection('providers').doc(pid))
  })
  batch.commit().then(result => {
    console.log(result)
  });
}

function getProviderIds(){
  db.collection('providers').get().then(qsnap => {
    console.log(qsnap.docs.map(d => d.id))
  })
}

// getProviderIds()
// removeProviders(['5tInyjouhlTkHKEbetkP', 'Zm1pK7M58MJ9WHWZU2cC'])
// parsePrices()
// parseProviders().then(addProviders)
addProvider()
// helpers.getGeocoding('904 Bayonne Crossing Way, Bayonne, NJ 07002').then(result => {
//   console.log(util.inspect(result, false, null, true))
// })
