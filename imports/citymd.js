// node import.js
var { db } = require('../clients/admin')
const helpers = require('../helpers')
const fs = require('fs')
var parse = require('csv-parse')

// CITY MD
// citymd-prices-parsed.js
function cityMDParsePrices(){
  fs.readFile('./import-data/citymd-prices.csv', (err, fileData) => {
    if(err){ console.error('Error', err); return }
    parse(fileData, {columns: true, trim: true}, (err, rows) => {
      if(err){ console.error('Error', err); return }
      let lists = {}
      rows.forEach(row => {
        let service = {
          name: row.Services,
          cost: helpers.toNumber(row.Price.slice(1)),
          time: helpers.toNumber(row.Time)
        }
        if(row.Default){
          service.isDefault = row.Default
        }
        if(row.Included){
          service.includes = row.Included.split(';')
        }
        if(!lists[row.PLID]) lists[row.PLID] = []
        lists[row.PLID].push(service)
      })
      // console.log(util.inspect(lists, false, null, true))
    })
  })
}

function cityMDParseProviders(){
  return new Promise(resolve => {
    var priceLists = require('./import-data/citymd-prices-parsed').priceLists
    fs.readFile('./import-data/citymd-locations.csv', (err, fileData) => {
      if(err){ console.error('Error', err); return }
      parse(fileData, {columns: true, trim: true}, (err, rows) => {
        var providers = rows.map(row => {
          console.log('parsing ' + row.ProvID)
          return helpers.getGeocoding(row.Address).then(office => {
            let provider = {
              providerInfo: {
                name: row.ProviderName,
                specialties: { primary: row.Specialty },
                rating: helpers.getRating(),
                photo: row.ImageLink
              },
              office
            }
            if(row.Phone){
              provider.providerInfo.phoneNumber = row.Phone
            }
            if(row.Description){
              provider.providerInfo.description = row.Description
            }
            if(row.Languages){
              provider.providerInfo.languages = helpers.splitTrim(row.Languages, ', ')
            }
            if(row.Training){
              provider.providerInfo.education = [ row.Training ]
            }
            let priceList = priceLists[row.PLID]
            if(!priceList){
              console.error('UHOH!', row)
              throw "No Price List!"
            }
            provider.services = priceList
            return provider
          })
        })
        // providers = array[promise]
        Promise.all(providers).then(resolve)
      })
    })
  })
}

function addProviders(providers){
  if(!Array.isArray(providers)){
    console.error('Providers is not an array', providers)
    throw 'uhoh'
  }
  console.log(`adding ${ providers.length } providers`)

  var batch = db.batch()
  providers.forEach(provider => {
    var newDoc = db.collection('providers').doc(helpers.getId())
    batch.create(newDoc, provider)
    // db.collection('providers').add(provider).then(ref => {
    //   console.log('Added: ' + ref.id)
    // })
  })
  batch.commit().then(result => {
    console.log(result)
  })
}

function removeProviders(providerIds){
  var batch = db.batch()
  providerIds.forEach(pid => {
    batch.delete(db.collection('providers').doc(pid))
  })
  batch.commit().then(result => {
    console.log(result)
  });
}

function getProviderIds(){
  db.collection('providers').get().then(qsnap => {
    console.log(qsnap.docs.map(d => d.id))
  })
}

// getProviderIds()
// removeProviders(['5tInyjouhlTkHKEbetkP', 'Zm1pK7M58MJ9WHWZU2cC'])

cityMDParseProviders().then(addProviders)

// helpers.getGeocoding('904 Bayonne Crossing Way, Bayonne, NJ 07002').then(result => {
//   console.log(util.inspect(result, false, null, true))
// })
