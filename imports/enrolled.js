// node import.js
const { admin, db } = require('../clients/admin')
var GeoFirestore = require('geofirestore').GeoFirestore
const helpers = require('../helpers')
const fs = require('fs')
var parse = require('csv-parse')
var geoRef = db.collection('geostore')
const geoFirestore = new GeoFirestore(geoRef);

// prohealth-prices-parsed.js
function parsePrices(){
  fs.readFile('./import-data/enrolled-prices.csv', (err, fileData) => {
    if(err){ console.error('Error', err); return }
    parse(fileData, {columns: true, trim: true}, (err, rows) => {
      if(err){ console.error('Error', err); return }
      let lists = {}
      rows.forEach(row => {
        let service = {
          name: row.Services,
          cost: helpers.toNumber(row.Price),
          time: helpers.toNumber(row.Time)
        }
        if(row.Default){
          service.isDefault = true
        }
        if(row.Includes){
          service.includes = helpers.splitTrim(row.Includes, ';')
        }
        if(!lists[row.ProvID]) lists[row.ProvID] = []
        lists[row.ProvID].push(service)
      })
      helpers.log(lists)
      // console.log(util.inspect(lists, false, null, true))
    })
  })
}

function parseProviders(){
  return new Promise(resolve => {
    var priceLists = require('../import-data/prices-parsed').enrolled
    fs.readFile('./import-data/enrolled-locations.csv', (err, fileData) => {
      if(err){ console.error('Error', err); return }
      parse(fileData, {columns: true, trim: true}, (err, rows) => {
        var providers = rows.map(row => {
          console.log('parsing ' + row.ProvID)
          return helpers.getGeocoding(row.Address).then(office => {
            let provider = {
              providerInfo: {
                name: row.Name,
                practiceName: row.Practice,
                specialties: { primary: row.Specialty },
                phoneNumber: row.Phone,
                email: row.Email,
                description: row.Description,
                languages: helpers.splitTrim(row.Languages, ','),
                rating: helpers.getRating(),
                photo: row.ImageLink,
              },
              office,
              createdOn: admin.firestore.FieldValue.serverTimestamp()
            }
            if(row.NPI){
              provider.providerInfo.npi = row.NPI
            }
            if(row.Degree){
              provider.providerInfo.degrees = row.Degree
            }
            if(row.Training){
              provider.providerInfo.education = helpers.splitTrim(row.Training, ';')
            }
            let priceList = priceLists[row.ProvID]
            if(!priceList){
              console.error('UHOH!', row)
              throw "No Price List!"
            }
            provider.services = priceList
            return provider
          })
        })
        // providers = array[promise]
        Promise.all(providers).then(resolve)
      })
    })
  })
}

function addProviders(providers){
  if(!Array.isArray(providers)){
    console.error('Providers is not an array', providers)
    throw 'uhoh'
  }
  console.log(`adding ${ providers.length } providers`)

  let coordMap = {}
  var batch = db.batch()
  providers.forEach(provider => {
    var providerId = helpers.getId()
    console.log('creating provider ' + providerId)
    var newDoc = db.collection('providers').doc(providerId)
    batch.create(newDoc, provider)
    if(!provider.office.location){
      console.error('uhoh', provider)
      throw 'geolocation'
    }
    let coordinates = new admin.firestore.GeoPoint(provider.office.location.lat, provider.office.location.lng)
    let specialty = provider.providerInfo.specialties.primary
    coordMap[providerId] = { coordinates, specialty }
    // db.collection('providers').add(provider).then(ref => {
    //   console.log('Added: ' + ref.id)
    // })
  })
  batch.commit().then(result => {
    console.log('BatchCommit: ', result, '-------')
    geoFirestore.set(coordMap).then(doc => {
      helpers.log(doc)
    })
  })
}

function removeProviders(providerIds){
  var batch = db.batch()
  providerIds.forEach(pid => {
    batch.delete(db.collection('providers').doc(pid))
  })
  batch.commit().then(result => {
    console.log(result)
  });
}

function getProviderIds(){
  db.collection('providers').get().then(qsnap => {
    console.log(qsnap.docs.map(d => d.id))
  })
}

// getProviderIds()
// removeProviders(['5tInyjouhlTkHKEbetkP', 'Zm1pK7M58MJ9WHWZU2cC'])
// parsePrices()
parseProviders().then(addProviders)

// helpers.getGeocoding('904 Bayonne Crossing Way, Bayonne, NJ 07002').then(result => {
//   console.log(util.inspect(result, false, null, true))
// })
