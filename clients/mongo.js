const MongoClient = require('mongodb').MongoClient;
const uri = process.env.MONGO_URI
let devClient

const db = mongoFunc => {
  if(devClient){
    console.log('doMongo - client exists')
    return new Promise(r => r(mongoFunc(devClient)))
  } else {
    console.log('doMongo - finding client')
    return MongoClient.connect(uri, { useNewUrlParser: true }).then(client => {
      console.log('client connected')
      devClient = client.db('dev')
      return mongoFunc(devClient)
    })
  }
}

const getCollection = collectionName => mongoFunc => {
  return db(db => mongoFunc(db.collection(collectionName)))
}

const ObjectId = require('mongodb').ObjectId

module.exports = { 
  db, 
  ObjectId,
  users: getCollection('users'),
  providers: getCollection('providers'),
  appointments: getCollection('appointments')
}
