const admin = require('firebase-admin')

const serviceAccount = require('./serviceAccountKey.json')
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://seethru-dev.firebaseio.com'
})

const auth = admin.auth()

const db = admin.firestore()
db.settings({ timestampsInSnapshots: true })


module.exports = { admin, db, auth }