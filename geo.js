// node index.js
const helpers = require('./helpers')
var { db, admin } = require('./clients/admin')
var GeoFirestore = require('geofirestore').GeoFirestore

var collectionRef = db.collection('geostore')
const geoFirestore = new GeoFirestore(collectionRef);

// Specialty: ''
function setCoords(){
  var coordMap = {}
  db.collection('providers').get().then(query => {
    query.forEach(doc => {
      let data = doc.data()
      if(!data.office.location){
        helpers.log(doc.data())
        throw 'huh?'
      }
      // let services = data.services.map(s => s.name)
      // .forEach(s => {
      //   services.push(s.name)
      //   if(s.includes){
      //     Array.prototype.push.apply(services, s.includes)
      //   }
      // })
      let coordinates = new admin.firestore.GeoPoint(data.office.location.lat, data.office.location.lng)
      let specialty = data.providerInfo.specialties.primary
      coordMap[doc.id] = { coordinates, specialty }
    })
    geoFirestore.set(coordMap).then(doc => {
      helpers.log(doc)
    })
  })
}

function queryCoordsAndSpecialty(skip, limit){
  const getSearchResults = () => new Promise((resolve, reject) => {
    var results = []
    var geoQuery = geoFirestore.query({
      center: new admin.firestore.GeoPoint(40.775, -73.912),
      radius: 30,
      query: (ref) => ref.where('d.specialty', '==', 'Urgent Care')
    })
    geoQuery.on('key_entered', function (key, document, distance) {
      // console.log(key + ' found at distance: ' + distance);
      results.push({ id: key, distance: distance / 1.609344 })
    })
    geoQuery.on('ready', function(){
      geoQuery.cancel()
      resolve(results)
    })
  })

  getSearchResults().then(searchResults => {
    console.log('-----------')
    if(skip >= searchResults.length){
      console.log('done skipping')
      return []
    }
    var orderedResults = searchResults.sort((a, b) => a.distance - b.distance)
    var limited = orderedResults.slice(skip, limit)
    console.log(limited)
    return limited
  })
}

// queryCoordsAndSpecialty(10, 20)


function querySpecialty(){ 
  collectionRef.where('d.specialty', '==', 'Urgent Care').limit(3)
    .get().then(query => {
      query.forEach( doc => {
        console.log(doc.id + ' found.')
      })
    })
}

setCoords()
// queryCoordsAndSpecialty()
// querySpecialty()

// Better structure
// providers: {
//   '00lPz81lZ6rywrDJTpQj': {
//     providerInfo: { },
//     office: {},
//     coordinates: GeoPoint
//   }
// }

// services: {
//   '00lPz81lZ6rywrDJTpQj': [

//   ]
// }