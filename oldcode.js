fs.readFile('myjsonfile.json', 'utf8', function readFileCallback(err, data){
    if (err){
        console.log(err);
    } else {
    obj = JSON.parse(data); //now it an object
    obj.table.push({id: 2, square:3}); //add some data
    json = JSON.stringify(obj); //convert it back to json
    fs.writeFile('myjsonfile.json', json, 'utf8', callback); // write it back 
}});

function parsePrices(){
  fs.readFile('./import-data/radnet-prices.csv', (err, fileData) => {
    parse(fileData, {columns: true, trim: true}, (err, rows) => {
      let rowsMap = {}
      let newRows = []
      rows.forEach(row => {
        let object = {
          CPTCode: row.CPTCode,
          Description: row.Description,
          Rate: row.Rate,
          Category: row.Category
        }
        if(!rowsMap[row.CPTCode]){
          rowsMap[row.CPTCode] = []
        }
        rowsMap[row.CPTCode].push(object)
      })
      Object.keys(rowsMap).forEach(cpt => {
        if(rowsMap[cpt].length > 1){
          var costs = rowsMap[cpt].map(r => helpers.toNumber(r.Rate))
          costs.sort( function(a,b) {return a - b;} );
          var half = Math.floor(costs.length/2);
          console.log('costs: ', costs, 'cost half', costs[half])
          var newRow = rowsMap[cpt].find(r => helpers.toNumber(r.Rate) === costs[half])
          console.log(newRow)
          newRows.push(newRow)
        } else {
          newRows.push(rowsMap[cpt][0])
        }
      })
      let csv = "CPTCode,Description,Rate,Category\n" + newRows.map(r => [
        r.CPTCode, r.Description, r.Rate, r.Category
      ].join(',')).join('\n')
      fs.writeFile('output.csv', csv, 'utf8', function(err) {
        if (err) { console.log('Some error occured - file either not saved or corrupted file saved.'); } 
        else { console.log('It`s saved!'); }
      });
    })
  })
}