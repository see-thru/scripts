// node index.js
const { admin, db, auth } = require('./clients/admin')
const mongo = require('./clients/mongo')
const helpers = require('./helpers')
const fs = require('fs')

// clean auth table
function cleanAuth(){
  auth.listUsers(50).then( listUsersResult => {
    listUsersResult.users.forEach(userRecord => {
      db.collection('user').doc(userRecord.uid).get().then(docSnap => {
        if(!docSnap.exists){
          console.log('user doesn‘t exist in table', userRecord.uid, userRecord.email)
          auth.deleteUser(userRecord.uid)
            .catch(e => console.error(e))
        }
      }).catch(e => console.error(e))
    });
  })
}

// clean user table
function cleanUsers(){
  auth.listUsers(50).then( listUsersResult => {
    var userIdsAuth = listUsersResult.users.map(u => u.uid)
    console.log('userIdsAuth', userIdsAuth)
  })

  db.collection('user').get().then(users => {
    var userIdsDb = users.docs.map(u => u.id)
    console.log(userIds)
  }).catch(e => console.error(e))
  
  userIdsDb.filter(id => userIdsAuth.indexOf(id) === -1)
  return
}

function fixProviderRatings(){
  var timestamp = new admin.firestore.Timestamp(1542902739, 0)
  db.collection('providers').where('createdOn', '>=', timestamp).get().then(qsnap => {
    qsnap.forEach(doc => {
      doc.ref.update({'providerInfo.rating': helpers.getRating()})
        .then(res => {
          console.log(`Document updated:`, res);
        })
    })
  })
}

function getProviderList(){
  db.collection('providers').get().then(qsnap => {
    var header = 'Provider ID,Name,Photo,App URL,Created On\n'
    var csv = qsnap.docs.map(p => {
      var data = p.data()
      var createdOn = data.createdOn ? data.createdOn.toDate().toGMTString() : ''
      return `${p.id},"${data.providerInfo.name}","${data.providerInfo.photo}",https://app.seethru.healthcare/provider/${p.id},"${createdOn}"`
    }).join('\n')
    fs.writeFile('providerList.csv', header + csv, 'utf8', function(err) {
      if (err) { console.log('Some error occured - file either not saved or corrupted file saved.'); } 
      else { console.log('It`s saved!'); }
    });
  })
}

function removeUserIds(userIds){
  var batch = db.batch()
  userIds.forEach(userId => {
    batch.delete(db.collection('user').doc(userId))
  })
  batch.commit().then( writeResult => {
    helpers.log(writeResult)
  })
}

// add new provider (from mock data)
function replaceImage(){
  db.collection('providers')
    .where('providerInfo.photo', '==', 'https://www.citymd.com/preview/citymd-com?repository=a926af9854fa794d7fef&branch=master&node=082f886164bbfcd9d540&size=1000&name=citymd&mimetype=image/png')
    .get()
    .then(qsnap => {
      qsnap.forEach(doc => {
        doc.ref.update({'providerInfo.photo': 'https://pbs.twimg.com/profile_images/716984640507154432/dbPvEcOb.jpg'})
          .then(res => {
            console.log(`Document updated:`, res);
          })
      })
    })
}

function fixServices(){
  var timestamp1 = new admin.firestore.Timestamp(1542906448, 0)
  var timestamp2 = new admin.firestore.Timestamp(1542906868, 0)

  db.collection('providers')
    .where('createdOn', '>', timestamp1)
    .where('createdOn', '<', timestamp2)
    .get()
    .then(qsnap => {
      qsnap.forEach(doc => {
        var services = doc.data().services.filter(s => (s.cptCode === 92004) || (s.cptCode === 92310))
        doc.ref.update({'services': services })
          .then(res => {
            console.log('Document updated: ', res)
          })
      })
    })
}

function downloadFirebaseTable(tableName){
  db.collection(tableName).get().then(qsnap => {
    var users = qsnap.docs.map(doc => {
      return Object.assign({ id: doc.id }, doc.data())
    })
    fs.writeFile(`./export/${tableName}.json`, JSON.stringify(users), 'utf8', function(err) {
      if (err) { console.log('Some error occured - file either not saved or corrupted file saved.'); } 
      else { console.log('It`s saved!'); }
    });
  })
}