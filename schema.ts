type User = {
  id: string,
  email: string,
  firstName: string,
  lastName: string,
  photo: string,
  dob: string
}

type Provider = {
  id: string,
  providerInfo: {
    name: string,
    photo: string,
    specialties: {
      primary: string,
      secondary: Array<string>
    },
    degrees: string,
    languages: Array<string>,
    training: Array<string>,
    practiceName: string,
    bio: string
  },
  office: {
    address: string,
    location: {
      lat: number,
      lng: number
    }
  },
  baseService: {
    name: string,
    time: number,
    cost: number,
    includes: Array<string>
  },
  // use subcollection - https://angularfirebase.com/lessons/firestore-nosql-data-modeling-by-example/
  services: Array<ProviderService>
}

type ProviderService = {
  // [serviceId]: number // serviceId -> cost
  id: string,
  name: string,
  time: number,
  cost: number,
  requiresVisit: boolean
}

type Service = {
  id: string,
  name: string,
  description: string,
  cptCode: string,
  time: number,
  defaultCost: number,
  keywords: string
}

type Bundle = {
  name: string,
  services: Array<string> // (service ids)
}